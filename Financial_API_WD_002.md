# Financial-grade API Security Profile 1.0 - Part 1: Advanced

This location previously contained the pre-final/draft of FAPI-RW / FAPI1-Advanced.

The final approved version of that document can be found here:

https://openid.net/specs/openid-financial-api-part-2-1_0.html

The latest working version of the document (which may contain errata not yet officially published) can be found here:

https://bitbucket.org/openid/fapi/src/master/FAPI_1.0/openid-financial-api-part-2-1_0.md
